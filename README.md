# Исследование влияния параметров запуска Prometheus на retention-политику

## Проблема

После загрузки в Prometheus исторических значений метрик в формате openmetrics Prometheus по непонятной причине удаляет блоки с добавленными метриками, неясным образом применяя retention-политики.

## Решение

Автоматизировать проверку влияния retention-параметров запуска prometheus на удаление им блоков (`Deleting obsolete block`) с загруженными историческими значениями метрик в openmetrics-формате.

## Реализация

Для проведения испытаний реализован Jupyter-ноутбук ([test.ipynb](./test.ipynb)).

Для работы Jupyter-ноутбука требуется установка библиотек:

```console
python -m pip install Jinja2 pandas pytz requests tqdm
```

Для запуска стендов — Docker Compose.

### Варианты запуска Prometheus

Проверяемые параметры запуска и их значения указываются в csv-файле ([retention-settings-cases.csv](./retention-settings-cases.csv)).

В столбце `comment` — описания вариантов запуска Prometheus.
В остальных столбцах — наименования аргументов Prometheus. В пересечениях столбцов и строк — значения аргументов.

Таблицу можно расширять произвольным образом, добавляя аргументы, варианты тестирования, значения аргументов.

| comment | enable-feature | storage.tsdb.retention.size | storage.tsdb.retention.time | storage.tsdb.retention |
|---|---|---|---|---|
| Default 15d retention applies |  |  |  |  |
| 20d retention applies |  |  | 20d |  |
| 10d retention applies |  |  |  | 10d |
| 20d retention applies |  |  | 20d | 10d |
| 1TB size retention applies, no time limit |  | 1TB |  |  |
| 1TB size and 20d time retention apply - which ever happens first |  | 1TB | 20d |  |
| 1TB size and 20d time retention apply - which ever happens first |  | 1TB | 20d | 10d |
| Default 15d retention applies + exemplar-storage | exemplar-storage |  |  |  |
| 20d retention applies + exemplar-storage | exemplar-storage |  | 20d |  |
| 10d retention applies + exemplar-storage | exemplar-storage |  |  | 10d |
| 20d retention applies + exemplar-storage | exemplar-storage |  | 20d | 10d |
| 1TB size retention applies, no time limit + exemplar-storage | exemplar-storage | 1TB |  |  |
| 1TB size and 20d time retention apply - which ever happens first + exemplar-storage | exemplar-storage | 1TB | 20d |  |
| 1TB size and 20d time retention apply - which ever happens first + exemplar-storage | exemplar-storage | 1TB | 20d | 10d |

### Исторические значения метрик

Загрузка исторических значений метрик в TSDB из файлов в openmetrics-формате осуществляется командами вида:

```console
promtool tsdb create-blocks-from openmetrics metrics.txt /prometheus/data
```

Для загрузки автоматизированно создаётся несколько файлов со значениями метрик разной степени давности:

* 1-one_hour_before.txt
* 2-one_week_before.txt
* 3-one_month_before.txt
* 4-one_year_before.txt
* 5-ten_years_before.txt

Для генерации файлов метрик в openmetrics-формате используется Jinja-шаблон:

```plain
# HELP {{ metric }}_http_requests_total The total number of HTTP requests.
# TYPE {{ metric }}_http_requests_total counter
{{ metric }}_http_requests_total{code="200",service="user"} 123 {{ timestamp }}
{{ metric }}_http_requests_total{code="500",service="user"} 456 {{ timestamp }}
# EOF
```

## Результаты

## Результаты запроса метрик в TSDB

| stand               | params                                                                                                                                                                      | metrics in TSDB                                                                                                                                                                             |
|---------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| prometheus-stand-1  | config.file=/etc/prometheus/prometheus.yml                                                                                                                                  | one_hour_before_http_requests_total, one_week_before_http_requests_total                                                                                                                  |
| prometheus-stand-2  | config.file=/etc/prometheus/prometheus.yml, storage.tsdb.retention.time=20d                                                                                               | one_hour_before_http_requests_total, one_week_before_http_requests_total                                                                                                                  |
| prometheus-stand-3  | config.file=/etc/prometheus/prometheus.yml, storage.tsdb.retention=10d                                                                                                    | one_hour_before_http_requests_total, one_week_before_http_requests_total                                                                                                                  |
| prometheus-stand-4  | config.file=/etc/prometheus/prometheus.yml, storage.tsdb.retention.time=20d, storage.tsdb.retention=10d                                                                   | one_hour_before_http_requests_total, one_week_before_http_requests_total                                                                                                                  |
| prometheus-stand-5  | config.file=/etc/prometheus/prometheus.yml, storage.tsdb.retention.size=1TB                                                                                               | one_hour_before_http_requests_total, one_month_before_http_requests_total, one_week_before_http_requests_total, one_year_before_http_requests_total, ten_years_before_http_requests_total |
| prometheus-stand-6  | config.file=/etc/prometheus/prometheus.yml, storage.tsdb.retention.size=1TB, storage.tsdb.retention.time=20d                                                              | one_hour_before_http_requests_total, one_week_before_http_requests_total                                                                                                                  |
| prometheus-stand-7  | config.file=/etc/prometheus/prometheus.yml, storage.tsdb.retention.size=1TB, storage.tsdb.retention.time=20d, storage.tsdb.retention=10d                                  | one_hour_before_http_requests_total, one_week_before_http_requests_total                                                                                                                  |
| prometheus-stand-8  | config.file=/etc/prometheus/prometheus.yml, enable-feature=exemplar-storage                                                                                               | one_hour_before_http_requests_total, one_week_before_http_requests_total                                                                                                                  |
| prometheus-stand-9  | config.file=/etc/prometheus/prometheus.yml, enable-feature=exemplar-storage, storage.tsdb.retention.time=20d                                                              | one_hour_before_http_requests_total, one_week_before_http_requests_total                                                                                                                  |
| prometheus-stand-10 | config.file=/etc/prometheus/prometheus.yml, enable-feature=exemplar-storage, storage.tsdb.retention=10d                                                                   | one_hour_before_http_requests_total, one_week_before_http_requests_total                                                                                                                  |
| prometheus-stand-11 | config.file=/etc/prometheus/prometheus.yml, enable-feature=exemplar-storage, storage.tsdb.retention.time=20d, storage.tsdb.retention=10d                                  | one_hour_before_http_requests_total, one_week_before_http_requests_total                                                                                                                  |
| prometheus-stand-12 | config.file=/etc/prometheus/prometheus.yml, enable-feature=exemplar-storage, storage.tsdb.retention.size=1TB                                                              | one_hour_before_http_requests_total, one_month_before_http_requests_total, one_week_before_http_requests_total, one_year_before_http_requests_total, ten_years_before_http_requests_total |
| prometheus-stand-13 | config.file=/etc/prometheus/prometheus.yml, enable-feature=exemplar-storage, storage.tsdb.retention.size=1TB, storage.tsdb.retention.time=20d                             | one_hour_before_http_requests_total, one_week_before_http_requests_total                                                                                                                  |
| prometheus-stand-14 | config.file=/etc/prometheus/prometheus.yml, enable-feature=exemplar-storage, storage.tsdb.retention.size=1TB, storage.tsdb.retention.time=20d, storage.tsdb.retention=10d | one_hour_before_http_requests_total, one_week_before_http_requests_total                                                                                                                  |

### Результаты анализа логов Prometheus

Удаление — даже частичное — загруженных в Prometheus метрик не происходит в случае, когда указан параметр:

```plain
storage.tsdb.retention.size=1TB
```
